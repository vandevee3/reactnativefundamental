/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Dimensions,
  Image,
  Button,
  TouchableOpacity,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';


const screen = Dimensions.get("screen");

const App: () => Node = () => {

  return (
    <>
      <View style={styles.maincontainer}>
        <View style={[styles.card, styles.shadowProp]}>
          <Image style = {styles.image} resizeMode = "cover" source={{uri: 'https://res.cloudinary.com/djyjm9ayd/image/upload/v1643249117/19025216_1439860629412992_3671167199250762358_o_hzryz8.png'}}/>
          <View style={styles.title}>
            <Text style={styles.mainTitle}>Styling di React Native</Text>
            <Text style={styles.secondaryTitle}>Binar Academy - React Native</Text>
          </View>
          <View style={styles.textContainer}>
            <Text style={styles.paragraphText}>
              As a component grows in complexity, it is much 
              cleaner and efficient to use StyleSheet.create 
              so as to define several styles in one piece.
            </Text>
          </View>
          <View style={styles.buttonContainer}>
            <TouchableOpacity >
              <Text style={styles.buttonStyle}>Understood!</Text>
            </TouchableOpacity>
            <TouchableOpacity >
              <Text style={styles.buttonStyle}>What???!</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  maincontainer : {
    flex : 1,
    justifyContent : 'center',
    alignItems : 'center',
  }, 
  card : {
    borderWidth : 0.4,
    borderColor : 'white',
    borderTopRightRadius : 10,
    borderBottomRightRadius : 10,
    borderTopLeftRadius : 10,
    borderBottomLeftRadius : 10,
    width: 0.8 * screen.width,
    padding :2,
  },
  shadowProp: {
    shadowColor: 'green',
    shadowOffset: {width: -2, height: 2},
    shadowOpacity: 0.3,
    // add shadows for Android only
    // No options for shadow color, shadow offset, shadow opacity like iOS
    elevation: 4,
  },
  image : {
    borderTopRightRadius : 10,
    borderTopLeftRadius : 10,
    height: 0.4 * screen.height,
  },
  title : {
    alignItems: 'center'
  },
  mainTitle : {
    fontSize : 20,
    color : 'black',
    paddingTop : 10
  },
  secondaryTitle : {
    letterSpacing : 2
  },
  textContainer : {
    marginTop : 5,
    padding : 10
  },
  paragraphText : {
    color: 'black'
  },
  buttonContainer :{
    flexDirection : 'row',
    justifyContent : 'space-around',
    marginTop : 10,
    marginBottom : 10
  },
  buttonStyle : {
    backgroundColor : 'white',
    color : 'green',
    fontSize : 20,
    paddingBottom : 10
  }
});

export default App;
